# Tengwar Fonts

I wanted a way to practice reading Tengwar. These fonts transcribe text into Tengwar. They can be used anywhere you can install fonts. 

<img src="https://gitlab.com/Varyaran/tengwar-fonts/-/raw/master/images/TengwarTelcontar/Android.png" width="30%">

<img src="https://gitlab.com/Varyaran/tengwar-fonts/-/raw/master/images/TengwarTelcontar/PC1.png" width="60%">

<img src="https://gitlab.com/Varyaran/tengwar-fonts/-/raw/master/images/TengwarTelcontar/PC2.png" width="60%">

The tengwar annatar fonts are incomplete, but they mostly work. The code I use is the same for all of them, but some glyphs are missing.

<img src="https://gitlab.com/Varyaran/tengwar-fonts/-/raw/master/images/TengwarAnnatar/Android.png" width="30%">

<img src="https://gitlab.com/Varyaran/tengwar-fonts/-/raw/master/images/TengwarAnnatar/PC1.png" width="60%">

<img src="https://gitlab.com/Varyaran/tengwar-fonts/-/raw/master/images/TengwarAnnatar/PC2.png" width="60%">
